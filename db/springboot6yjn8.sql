/*
 Navicat Premium Data Transfer

 Source Server         : 本机MySQL
 Source Server Type    : MySQL
 Source Server Version : 50744
 Source Host           : localhost:3306
 Source Schema         : springboot6yjn8

 Target Server Type    : MySQL
 Target Server Version : 50744
 File Encoding         : 65001

 Date: 26/03/2024 12:16:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for application_management
-- ----------------------------
DROP TABLE IF EXISTS `application_management`;
CREATE TABLE `application_management`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `original_school_account` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '院校账号',
  `responsible_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人姓名',
  `user_account` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户账号',
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `notification_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '通知内容',
  `send_time` datetime NULL DEFAULT NULL COMMENT '发送时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1711391965622 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知推送' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of application_management
-- ----------------------------
INSERT INTO `application_management` VALUES (1711125579426, '2024-03-23 00:39:39', NULL, '李博涛', '李四', '4311101245135422', '家长看望学生', '2024-03-23 00:39:24');
INSERT INTO `application_management` VALUES (1711391965621, '2024-03-26 02:39:25', NULL, '12', '1', '111111111111111111', '22', '2024-03-04 00:00:00');

-- ----------------------------
-- Table structure for car_category
-- ----------------------------
DROP TABLE IF EXISTS `car_category`;
CREATE TABLE `car_category`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `unit_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位类别',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1711117734843 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单位类别' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of car_category
-- ----------------------------
INSERT INTO `car_category` VALUES (1711117724440, '2024-03-22 22:28:43', '保卫科');
INSERT INTO `car_category` VALUES (1711117734842, '2024-03-22 22:28:54', '后勤处');

-- ----------------------------
-- Table structure for car_management
-- ----------------------------
DROP TABLE IF EXISTS `car_management`;
CREATE TABLE `car_management`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `gender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `age` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年龄',
  `photo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '照片',
  `personal_info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个人资料',
  `unit_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位类别',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `contact_info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1711126797307 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单位' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of car_management
-- ----------------------------
INSERT INTO `car_management` VALUES (1711126797306, '2024-03-23 00:59:56', '大学校车', '是', '刘可', 'http://localhost:8080/springboot6yjn8/upload/1711126785719.png', NULL, '保卫科', NULL, '13222121234');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置参数名称',
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置参数值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '配置文件' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES (1, 'picture1', 'http://localhost:8080/springboot6yjn8/upload/picture1.jpg');
INSERT INTO `config` VALUES (2, 'picture2', 'http://localhost:8080/springboot6yjn8/upload/picture2.jpg');
INSERT INTO `config` VALUES (3, 'picture3', 'http://localhost:8080/springboot6yjn8/upload/picture3.jpg');
INSERT INTO `config` VALUES (6, 'homepage', NULL);

-- ----------------------------
-- Table structure for doorkeeper
-- ----------------------------
DROP TABLE IF EXISTS `doorkeeper`;
CREATE TABLE `doorkeeper`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `user_account` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `password` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `gender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `age` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年龄',
  `personal_info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个人资料',
  `photo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '照片',
  `contact_info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `unit_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位类别',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `yonghuzhanghao`(`user_account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1711120213871 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of doorkeeper
-- ----------------------------
INSERT INTO `doorkeeper` VALUES (21, '2024-03-22 11:07:09', '用户1', '123456', '用户姓名1', '男', '年龄1', '', 'http://localhost:8080/springboot6yjn8/upload/yonghu_zhaopian1.jpg', '联系方式1', '保卫科');
INSERT INTO `doorkeeper` VALUES (22, '2024-03-22 11:07:09', '用户2', '123456', '用户姓名2', '男', '年龄2', '', 'http://localhost:8080/springboot6yjn8/upload/yonghu_zhaopian2.jpg', '联系方式2', '保卫科');
INSERT INTO `doorkeeper` VALUES (23, '2024-03-22 11:07:09', '用户3', '123456', '用户姓名3', '男', '年龄3', '', 'http://localhost:8080/springboot6yjn8/upload/yonghu_zhaopian3.jpg', '联系方式3', '保卫科');
INSERT INTO `doorkeeper` VALUES (24, '2024-03-22 11:07:09', '用户4', '123456', '用户姓名4', '男', '年龄4', '', 'http://localhost:8080/springboot6yjn8/upload/yonghu_zhaopian4.jpg', '联系方式4', '保卫科');
INSERT INTO `doorkeeper` VALUES (25, '2024-03-22 11:07:09', '用户5', '123456', '用户姓名5', '男', '年龄5', '', 'http://localhost:8080/springboot6yjn8/upload/yonghu_zhaopian5.jpg', '联系方式5', '保卫科');
INSERT INTO `doorkeeper` VALUES (26, '2024-03-22 11:07:09', '用户6', '123456', '用户姓名6', '男', '年龄6', '', 'http://localhost:8080/springboot6yjn8/upload/yonghu_zhaopian6.jpg', '联系方式6', '保卫科');
INSERT INTO `doorkeeper` VALUES (1615272724814, '2024-03-22 11:07:09', '1', '1', 'xx用户', '性别1', '年龄1', 'http://localhost:8080/springboot6yjn8/upload/1615272723412.docx', 'http://localhost:8080/springboot6yjn8/upload/danwei_zhaopian1.jpg', '联系方式1', '保卫科');
INSERT INTO `doorkeeper` VALUES (1711120213870, '2024-03-22 23:10:13', '12', '1', '1', '性别1', '年龄1', 'http://localhost:8080/springboot6yjn8/upload/1615272743383.docx', 'http://localhost:8080/springboot6yjn8/upload/danwei_zhaopian1.jpg', '联系方式1', '单位类别1');

-- ----------------------------
-- Table structure for duty
-- ----------------------------
DROP TABLE IF EXISTS `duty`;
CREATE TABLE `duty`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `original_school_account` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '院校账号',
  `password` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `responsible_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人姓名',
  `gender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `contact_info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1711389471158 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '院校管理员' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of duty
-- ----------------------------
INSERT INTO `duty` VALUES (1, '2024-03-26 01:56:23', '1', '1', '1', '1', 1, '1', '1');
INSERT INTO `duty` VALUES (1711389471157, '2024-03-26 01:57:51', '2', '3', '3', '是', 3, '3', '3');

-- ----------------------------
-- Table structure for notification_reply
-- ----------------------------
DROP TABLE IF EXISTS `notification_reply`;
CREATE TABLE `notification_reply`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `original_school_account` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '院校账号',
  `user_account` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户账号',
  `reply_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '回复内容',
  `reply_time` datetime NULL DEFAULT NULL COMMENT '回复时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1615272812836 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知回复' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of notification_reply
-- ----------------------------
INSERT INTO `notification_reply` VALUES (81, '2024-03-22 11:07:09', '院校账号1', '用户账号1', '回复内容1', '2024-03-22 11:07:09');
INSERT INTO `notification_reply` VALUES (82, '2024-03-22 11:07:09', '院校账号2', '用户账号2', '回复内容2', '2024-03-22 11:07:09');
INSERT INTO `notification_reply` VALUES (83, '2024-03-22 11:07:09', '院校账号3', '用户账号3', '回复内容3', '2024-03-22 11:07:09');
INSERT INTO `notification_reply` VALUES (84, '2024-03-22 11:07:09', '院校账号4', '用户账号4', '回复内容4', '2024-03-22 11:07:09');
INSERT INTO `notification_reply` VALUES (85, '2024-03-22 11:07:09', '院校账号5', '用户账号5', '回复内容5', '2024-03-22 11:07:09');
INSERT INTO `notification_reply` VALUES (86, '2024-03-22 11:07:09', '院校账号6', '用户账号6', '回复内容6', '2024-03-22 11:07:09');
INSERT INTO `notification_reply` VALUES (1615272812835, '2024-03-22 11:07:09', '1', '1', 'asdas', '2024-03-22 11:07:09');

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userid` bigint(20) NOT NULL COMMENT '用户id',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `tablename` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表名',
  `role` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色',
  `token` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '新增时间',
  `expiratedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '过期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'token表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of token
-- ----------------------------
INSERT INTO `token` VALUES (1, 1, 'abo', 'users', '管理员', 'jma1n7rz3ug52c9p6r1sbe5awudt4s6q', '2024-03-22 11:07:09', '2024-03-26 13:01:53');
INSERT INTO `token` VALUES (2, 1615272724814, '1', 'yonghu', '用户', '3oxj1dkuebip00gjelsu8o25ctv5r5rr', '2024-03-22 11:07:09', '2024-03-22 11:07:09');
INSERT INTO `token` VALUES (3, 41, '1', 'yuanxiaoguanliyuan', '院校管理员', 'sf45oqjag6tfvg10rsatso3lhepm68tq', '2024-03-22 11:07:09', '2024-03-22 22:43:59');
INSERT INTO `token` VALUES (4, 21, '用户1', 'yonghu', '用户', '86b7s5e28fyqha33cvbsjfkrzanj8ri5', '2024-03-22 21:44:55', '2024-03-22 22:44:55');
INSERT INTO `token` VALUES (5, 21, '用户1', 'yonghu', '门卫', '7g76d0pojm3omn6zzuw6axn4hecde9uu', '2024-03-23 00:20:47', '2024-03-23 02:39:28');
INSERT INTO `token` VALUES (6, 1615272724814, '1', 'front_user', '门卫', 'i8ctp39scs9j2ppl0zgu1rrosf93usg6', '2024-03-26 02:23:10', '2024-03-26 13:14:08');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `role` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '管理员' COMMENT '角色',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '新增时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'abo', 'abo', '管理员', '2024-03-22 11:07:09');

-- ----------------------------
-- Table structure for visitor_management
-- ----------------------------
DROP TABLE IF EXISTS `visitor_management`;
CREATE TABLE `visitor_management`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `original_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学院名称',
  `file_data` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资料文件',
  `original_desc` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '学院简介',
  `worker_num` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职工人数',
  `original_school_account` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '院校账号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1711118967218 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '院校' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of visitor_management
-- ----------------------------
INSERT INTO `visitor_management` VALUES (1711118967217, '2024-03-22 22:49:26', '张三', 'http://localhost:8080/springboot6yjn8/upload/1711390375090.png', '收垃圾', '收垃圾', '111');

-- ----------------------------
-- Table structure for vote_data
-- ----------------------------
DROP TABLE IF EXISTS `vote_data`;
CREATE TABLE `vote_data`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `candidateName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '候选人姓名',
  `gender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `age` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年龄',
  `personal` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '个人介绍',
  `approve_vote` int(11) NULL DEFAULT NULL COMMENT '赞成票',
  `oppose_vote` int(11) NULL DEFAULT NULL COMMENT '反对票',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '投票信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of vote_data
-- ----------------------------
INSERT INTO `vote_data` VALUES (71, '2024-03-22 11:07:09', '候选人姓名1', '男', '年龄1', '个人介绍1', 1, 1, '2024-03-22 11:07:09');
INSERT INTO `vote_data` VALUES (72, '2024-03-22 11:07:09', '候选人姓名2', '男', '年龄2', '个人介绍2', 2, 2, '2024-03-22 11:07:09');
INSERT INTO `vote_data` VALUES (73, '2024-03-22 11:07:09', '候选人姓名3', '男', '年龄3', '个人介绍3', 3, 3, '2024-03-22 11:07:09');
INSERT INTO `vote_data` VALUES (74, '2024-03-22 11:07:09', '候选人姓名4', '男', '年龄4', '个人介绍4', 4, 4, '2024-03-22 11:07:09');
INSERT INTO `vote_data` VALUES (75, '2024-03-22 11:07:09', '候选人姓名5', '男', '年龄5', '个人介绍5', 5, 5, '2024-03-22 11:07:09');
INSERT INTO `vote_data` VALUES (76, '2024-03-22 11:07:09', '候选人姓名6', '男', '年龄6', '个人介绍6', 6, 6, '2024-03-22 11:07:09');

SET FOREIGN_KEY_CHECKS = 1;
