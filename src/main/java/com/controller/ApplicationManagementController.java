package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;

import com.entity.ApplicationManagementEntity;
import com.entity.view.ApplicationManagementView;

import com.service.ApplicationManagementService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;


/**
 * 入校申请
 * 后端接口
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@RestController
@RequestMapping("/application_management")
public class ApplicationManagementController {
    @Autowired
    private ApplicationManagementService applicationManagementService;
    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params, ApplicationManagementEntity notificationSend, HttpServletRequest request){
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("duty")) {
			notificationSend.setOriginalSchoolAccount((String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("doorkeeper")) {
			notificationSend.setUserAccount((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<ApplicationManagementEntity> ew = new EntityWrapper<ApplicationManagementEntity>();
		PageUtils page = applicationManagementService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, notificationSend), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params, ApplicationManagementEntity notificationSend, HttpServletRequest request){
        EntityWrapper<ApplicationManagementEntity> ew = new EntityWrapper<ApplicationManagementEntity>();
		PageUtils page = applicationManagementService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, notificationSend), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( ApplicationManagementEntity notificationSend){
       	EntityWrapper<ApplicationManagementEntity> ew = new EntityWrapper<ApplicationManagementEntity>();
      	ew.allEq(MPUtil.allEQMapPre( notificationSend, "application_management")); 
        return R.ok().put("data", applicationManagementService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(ApplicationManagementEntity notificationSend){
        EntityWrapper<ApplicationManagementEntity> ew = new EntityWrapper<ApplicationManagementEntity>();
 		ew.allEq(MPUtil.allEQMapPre( notificationSend, "application_management")); 
		ApplicationManagementView notificationSendView =  applicationManagementService.selectView(ew);
		return R.ok("查询入校申请成功").put("data", notificationSendView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        ApplicationManagementEntity notificationSend = applicationManagementService.selectById(id);
        return R.ok().put("data", notificationSend);
    }

    /**
     * 前端详情
     */
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        ApplicationManagementEntity notificationSend = applicationManagementService.selectById(id);
        return R.ok().put("data", notificationSend);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ApplicationManagementEntity notificationSend, HttpServletRequest request){
    	notificationSend.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(notificationSend);
		notificationSend.setAuditStatus("申请中");
        applicationManagementService.insert(notificationSend);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody ApplicationManagementEntity notificationSend, HttpServletRequest request){
    	notificationSend.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(notificationSend);
        applicationManagementService.insert(notificationSend);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody ApplicationManagementEntity notificationSend, HttpServletRequest request){
        //ValidatorUtils.validateEntity(notificationSend);
        applicationManagementService.updateById(notificationSend);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        applicationManagementService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<ApplicationManagementEntity> wrapper = new EntityWrapper<ApplicationManagementEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}

		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("duty")) {
			wrapper.eq("original_school_account", (String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("doorkeeper")) {
			wrapper.eq("user_account", (String)request.getSession().getAttribute("username"));
		}

		int count = applicationManagementService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	


}
