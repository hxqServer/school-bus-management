package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;

import com.entity.CarCategoryEntity;
import com.entity.view.CarCategoryView;

import com.service.CarCategoryService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;


/**
 * 校车类别
 * 后端接口
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@RestController
@RequestMapping("/car_category")
public class CarCategoryController {
    @Autowired
    private CarCategoryService carCategoryService;
    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params, CarCategoryEntity unitType, HttpServletRequest request){
        EntityWrapper<CarCategoryEntity> ew = new EntityWrapper<CarCategoryEntity>();
		PageUtils page = carCategoryService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, unitType), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params, CarCategoryEntity unitType, HttpServletRequest request){
        EntityWrapper<CarCategoryEntity> ew = new EntityWrapper<CarCategoryEntity>();
		PageUtils page = carCategoryService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, unitType), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( CarCategoryEntity unitType){
       	EntityWrapper<CarCategoryEntity> ew = new EntityWrapper<CarCategoryEntity>();
      	ew.allEq(MPUtil.allEQMapPre( unitType, "unitType")); 
        return R.ok().put("data", carCategoryService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(CarCategoryEntity unitType){
        EntityWrapper<CarCategoryEntity> ew = new EntityWrapper<CarCategoryEntity>();
 		ew.allEq(MPUtil.allEQMapPre( unitType, "unitType")); 
		CarCategoryView unitTypeView =  carCategoryService.selectView(ew);
		return R.ok("查询校车类别成功").put("data", unitTypeView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        CarCategoryEntity unitType = carCategoryService.selectById(id);
        return R.ok().put("data", unitType);
    }

    /**
     * 前端详情
     */
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        CarCategoryEntity unitType = carCategoryService.selectById(id);
        return R.ok().put("data", unitType);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CarCategoryEntity unitType, HttpServletRequest request){
    	unitType.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(unitType);
        carCategoryService.insert(unitType);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody CarCategoryEntity unitType, HttpServletRequest request){
    	unitType.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(unitType);
        carCategoryService.insert(unitType);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CarCategoryEntity unitType, HttpServletRequest request){
        //ValidatorUtils.validateEntity(unitType);
        carCategoryService.updateById(unitType);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        carCategoryService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<CarCategoryEntity> wrapper = new EntityWrapper<CarCategoryEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}


		int count = carCategoryService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	


}
