package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;

import com.entity.CarManagementEntity;
import com.entity.view.CarManagementView;

import com.service.CarManagementService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;


/**
 * 校车
 * 后端接口
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@RestController
@RequestMapping("/car_management")
public class CarManagementController {
    @Autowired
    private CarManagementService carManagementService;
    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params, CarManagementEntity car_management, HttpServletRequest request){
        EntityWrapper<CarManagementEntity> ew = new EntityWrapper<CarManagementEntity>();
		PageUtils page = carManagementService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, car_management), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params, CarManagementEntity car_management, HttpServletRequest request){
        EntityWrapper<CarManagementEntity> ew = new EntityWrapper<CarManagementEntity>();
		PageUtils page = carManagementService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, car_management), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( CarManagementEntity car_management){
       	EntityWrapper<CarManagementEntity> ew = new EntityWrapper<CarManagementEntity>();
      	ew.allEq(MPUtil.allEQMapPre( car_management, "car_management")); 
        return R.ok().put("data", carManagementService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(CarManagementEntity car_management){
        EntityWrapper<CarManagementEntity> ew = new EntityWrapper<CarManagementEntity>();
 		ew.allEq(MPUtil.allEQMapPre( car_management, "car_management")); 
		CarManagementView danweiView =  carManagementService.selectView(ew);
		return R.ok("查询校车成功").put("data", danweiView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        CarManagementEntity car_management = carManagementService.selectById(id);
        return R.ok().put("data", car_management);
    }

    /**
     * 前端详情
     */
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        CarManagementEntity car_management = carManagementService.selectById(id);
        return R.ok().put("data", car_management);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CarManagementEntity car_management, HttpServletRequest request){
    	car_management.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(car_management);
        carManagementService.insert(car_management);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody CarManagementEntity car_management, HttpServletRequest request){
    	car_management.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(car_management);
        carManagementService.insert(car_management);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CarManagementEntity car_management, HttpServletRequest request){
        //ValidatorUtils.validateEntity(car_management);
        carManagementService.updateById(car_management);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        carManagementService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<CarManagementEntity> wrapper = new EntityWrapper<CarManagementEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}


		int count = carManagementService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	


}
