package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.DoorKeeperEntity;
import com.entity.view.DoorKeeperView;

import com.service.DoorKeeperService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;


/**
 * 门卫
 * 后端接口
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@RestController
@RequestMapping("/doorkeeper")
public class DoorKeeperController {
    @Autowired
    private DoorKeeperService doorKeeperService;
    
	@Autowired
	private TokenService tokenService;
	
	/**
	 * 登录
	 */
	@IgnoreAuth
	@RequestMapping(value = "/login")
	public R login(String username, String password, String captcha, HttpServletRequest request) {
		DoorKeeperEntity user = doorKeeperService.selectOne(new EntityWrapper<DoorKeeperEntity>().eq("user_account", username));
		if(user==null || !user.getPassword().equals(password)) {
			return R.error("账号或密码不正确");
		}
		
		String token = tokenService.generateToken(user.getId(), username,"doorkeeper",  "门卫" );
		return R.ok().put("token", token);
	}
	
	/**
     * 注册
     */
	@IgnoreAuth
    @RequestMapping("/register")
    public R register(@RequestBody DoorKeeperEntity frontUser){
    	//ValidatorUtils.validateEntity(frontUser);
    	DoorKeeperEntity user = doorKeeperService.selectOne(new EntityWrapper<DoorKeeperEntity>().eq("user_account", frontUser.getUserAccount()));
		if(user!=null) {
			return R.error("注册门卫已存在");
		}
		Long uId = new Date().getTime();
		frontUser.setId(uId);
        doorKeeperService.insert(frontUser);
        return R.ok();
    }
	
	/**
	 * 退出
	 */
	@RequestMapping("/logout")
	public R logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return R.ok("退出成功");
	}
	
	/**
     * 获取门卫的session门卫信息
     */
    @RequestMapping("/session")
    public R getCurrUser(HttpServletRequest request){
    	Long id = (Long)request.getSession().getAttribute("userId");
        DoorKeeperEntity user = doorKeeperService.selectById(id);
        return R.ok().put("data", user);
    }
    
    /**
     * 密码重置
     */
    @IgnoreAuth
	@RequestMapping(value = "/resetPass")
    public R resetPass(String username, HttpServletRequest request){
    	DoorKeeperEntity user = doorKeeperService.selectOne(new EntityWrapper<DoorKeeperEntity>().eq("user_account", username));
    	if(user==null) {
    		return R.error("账号不存在");
    	}
        user.setPassword("123456");
        doorKeeperService.updateById(user);
        return R.ok("密码已重置为：123456");
    }


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params, DoorKeeperEntity frontUser, HttpServletRequest request){
        EntityWrapper<DoorKeeperEntity> ew = new EntityWrapper<DoorKeeperEntity>();
		PageUtils page = doorKeeperService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, frontUser), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params, DoorKeeperEntity frontUser, HttpServletRequest request){
        EntityWrapper<DoorKeeperEntity> ew = new EntityWrapper<DoorKeeperEntity>();
		PageUtils page = doorKeeperService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, frontUser), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( DoorKeeperEntity frontUser){
       	EntityWrapper<DoorKeeperEntity> ew = new EntityWrapper<DoorKeeperEntity>();
      	ew.allEq(MPUtil.allEQMapPre( frontUser, "doorkeeper")); 
        return R.ok().put("data", doorKeeperService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(DoorKeeperEntity frontUser){
        EntityWrapper<DoorKeeperEntity> ew = new EntityWrapper<DoorKeeperEntity>();
 		ew.allEq(MPUtil.allEQMapPre( frontUser, "doorkeeper")); 
		DoorKeeperView frontUserView =  doorKeeperService.selectView(ew);
		return R.ok("查询门卫成功").put("data", frontUserView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        DoorKeeperEntity frontUser = doorKeeperService.selectById(id);
        return R.ok().put("data", frontUser);
    }

    /**
     * 前端详情
     */
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        DoorKeeperEntity frontUser = doorKeeperService.selectById(id);
        return R.ok().put("data", frontUser);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody DoorKeeperEntity frontUser, HttpServletRequest request){
    	frontUser.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(frontUser);
    	DoorKeeperEntity user = doorKeeperService.selectOne(new EntityWrapper<DoorKeeperEntity>().eq("user_account", frontUser.getUserAccount()));
		if(user!=null) {
			return R.error("门卫已存在");
		}
		frontUser.setId(new Date().getTime());
        doorKeeperService.insert(frontUser);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody DoorKeeperEntity frontUser, HttpServletRequest request){
    	frontUser.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(frontUser);
    	DoorKeeperEntity user = doorKeeperService.selectOne(new EntityWrapper<DoorKeeperEntity>().eq("user_account", frontUser.getUserAccount()));
		if(user!=null) {
			return R.error("门卫已存在");
		}
		frontUser.setId(new Date().getTime());
        doorKeeperService.insert(frontUser);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody DoorKeeperEntity frontUser, HttpServletRequest request){
        //ValidatorUtils.validateEntity(frontUser);
        doorKeeperService.updateById(frontUser);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        doorKeeperService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<DoorKeeperEntity> wrapper = new EntityWrapper<DoorKeeperEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}


		int count = doorKeeperService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	


}
