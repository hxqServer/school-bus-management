package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.DutyEntity;
import com.entity.view.DutyView;

import com.service.DutyService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;


/**
 * 访客管理员
 * 后端接口
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@RestController
@RequestMapping("/duty")
public class DutyController {
    @Autowired
    private DutyService dutyService;
    
	@Autowired
	private TokenService tokenService;
	
	/**
	 * 登录
	 */
	@IgnoreAuth
	@RequestMapping(value = "/login")
	public R login(String username, String password, String captcha, HttpServletRequest request) {
		DutyEntity user = dutyService.selectOne(new EntityWrapper<DutyEntity>().eq("original_school_account", username));
		if(user==null || !user.getPassword().equals(password)) {
			return R.error("账号或密码不正确");
		}
		
		String token = tokenService.generateToken(user.getId(), username,"duty",  "访客管理员" );
		return R.ok().put("token", token);
	}
	
	/**
     * 注册
     */
	@IgnoreAuth
    @RequestMapping("/register")
    public R register(@RequestBody DutyEntity originalAdmin){
    	//ValidatorUtils.validateEntity(originalAdmin);
    	DutyEntity user = dutyService.selectOne(new EntityWrapper<DutyEntity>().eq("original_school_account", originalAdmin.getOriginalSchoolAccount()));
		if(user!=null) {
			return R.error("注册门卫已存在");
		}
		Long uId = new Date().getTime();
		originalAdmin.setId(uId);
        dutyService.insert(originalAdmin);
        return R.ok();
    }
	
	/**
	 * 退出
	 */
	@RequestMapping("/logout")
	public R logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return R.ok("退出成功");
	}
	
	/**
     * 获取门卫的session门卫信息
     */
    @RequestMapping("/session")
    public R getCurrUser(HttpServletRequest request){
    	Long id = (Long)request.getSession().getAttribute("userId");
        DutyEntity user = dutyService.selectById(id);
        return R.ok().put("data", user);
    }
    
    /**
     * 密码重置
     */
    @IgnoreAuth
	@RequestMapping(value = "/resetPass")
    public R resetPass(String username, HttpServletRequest request){
    	DutyEntity user = dutyService.selectOne(new EntityWrapper<DutyEntity>().eq("original_school_account", username));
    	if(user==null) {
    		return R.error("账号不存在");
    	}
        user.setPasswordsword("123456");
        dutyService.updateById(user);
        return R.ok("密码已重置为：123456");
    }


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params, DutyEntity originalAdmin, HttpServletRequest request){
        EntityWrapper<DutyEntity> ew = new EntityWrapper<DutyEntity>();
		PageUtils page = dutyService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, originalAdmin), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params, DutyEntity originalAdmin, HttpServletRequest request){
        EntityWrapper<DutyEntity> ew = new EntityWrapper<DutyEntity>();
		PageUtils page = dutyService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, originalAdmin), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( DutyEntity originalAdmin){
       	EntityWrapper<DutyEntity> ew = new EntityWrapper<DutyEntity>();
      	ew.allEq(MPUtil.allEQMapPre( originalAdmin, "duty")); 
        return R.ok().put("data", dutyService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(DutyEntity originalAdmin){
        EntityWrapper<DutyEntity> ew = new EntityWrapper<DutyEntity>();
 		ew.allEq(MPUtil.allEQMapPre( originalAdmin, "duty")); 
		DutyView originalAdminView =  dutyService.selectView(ew);
		return R.ok("查询访客管理员成功").put("data", originalAdminView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        DutyEntity originalAdmin = dutyService.selectById(id);
        return R.ok().put("data", originalAdmin);
    }

    /**
     * 前端详情
     */
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        DutyEntity originalAdmin = dutyService.selectById(id);
        return R.ok().put("data", originalAdmin);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody DutyEntity originalAdmin, HttpServletRequest request){
    	originalAdmin.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(originalAdmin);
//    	DutyEntity user = dutyService.selectOne(new EntityWrapper<DutyEntity>().eq("original_school_account", originalAdmin.getOriginalSchoolAccount()));
//		if(user!=null) {
//			return R.error("门卫已存在");
//		}
		originalAdmin.setId(new Date().getTime());
        dutyService.insert(originalAdmin);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody DutyEntity originalAdmin, HttpServletRequest request){
    	originalAdmin.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(originalAdmin);
    	DutyEntity user = dutyService.selectOne(new EntityWrapper<DutyEntity>().eq("original_school_account", originalAdmin.getOriginalSchoolAccount()));
		if(user!=null) {
			return R.error("门卫已存在");
		}
		originalAdmin.setId(new Date().getTime());
        dutyService.insert(originalAdmin);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody DutyEntity originalAdmin, HttpServletRequest request){
        //ValidatorUtils.validateEntity(originalAdmin);
        dutyService.updateById(originalAdmin);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        dutyService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<DutyEntity> wrapper = new EntityWrapper<DutyEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}


		int count = dutyService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	


}
