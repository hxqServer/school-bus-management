package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;

import com.entity.VisitorManagementEntity;
import com.entity.view.VisitorManagementView;

import com.service.VisitorManagementService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;


/**
 * 访客
 * 后端接口
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@RestController
@RequestMapping("/visitor_management")
public class VisitorManagementController {
    @Autowired
    private VisitorManagementService visitorManagementService;
    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params, VisitorManagementEntity yuanxiao, HttpServletRequest request){
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("duty")) {
			yuanxiao.setOriginalSchoolAccount((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<VisitorManagementEntity> ew = new EntityWrapper<VisitorManagementEntity>();
		PageUtils page = visitorManagementService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, yuanxiao), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params, VisitorManagementEntity yuanxiao, HttpServletRequest request){
        EntityWrapper<VisitorManagementEntity> ew = new EntityWrapper<VisitorManagementEntity>();
		PageUtils page = visitorManagementService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, yuanxiao), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( VisitorManagementEntity yuanxiao){
       	EntityWrapper<VisitorManagementEntity> ew = new EntityWrapper<VisitorManagementEntity>();
      	ew.allEq(MPUtil.allEQMapPre( yuanxiao, "visitor_management")); 
        return R.ok().put("data", visitorManagementService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(VisitorManagementEntity yuanxiao){
        EntityWrapper<VisitorManagementEntity> ew = new EntityWrapper<VisitorManagementEntity>();
 		ew.allEq(MPUtil.allEQMapPre( yuanxiao, "visitor_management")); 
		VisitorManagementView yuanxiaoView =  visitorManagementService.selectView(ew);
		return R.ok("查询访客成功").put("data", yuanxiaoView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        VisitorManagementEntity yuanxiao = visitorManagementService.selectById(id);
        return R.ok().put("data", yuanxiao);
    }

    /**
     * 前端详情
     */
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        VisitorManagementEntity yuanxiao = visitorManagementService.selectById(id);
        return R.ok().put("data", yuanxiao);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody VisitorManagementEntity yuanxiao, HttpServletRequest request){
    	yuanxiao.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(yuanxiao);
        visitorManagementService.insert(yuanxiao);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody VisitorManagementEntity yuanxiao, HttpServletRequest request){
    	yuanxiao.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(yuanxiao);
        visitorManagementService.insert(yuanxiao);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody VisitorManagementEntity yuanxiao, HttpServletRequest request){
        //ValidatorUtils.validateEntity(yuanxiao);
        visitorManagementService.updateById(yuanxiao);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        visitorManagementService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<VisitorManagementEntity> wrapper = new EntityWrapper<VisitorManagementEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}

		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("duty")) {
			wrapper.eq("original_school_account", (String)request.getSession().getAttribute("username"));
		}

		int count = visitorManagementService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	


}
