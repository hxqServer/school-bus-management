package com.dao;

import com.entity.ApplicationManagementEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.ApplicationManagementVO;
import com.entity.view.ApplicationManagementView;


/**
 * 入校申请
 * 
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface ApplicationManagementDao extends BaseMapper<ApplicationManagementEntity> {
	
	List<ApplicationManagementVO> selectListVO(@Param("ew") Wrapper<ApplicationManagementEntity> wrapper);
	
	ApplicationManagementVO selectVO(@Param("ew") Wrapper<ApplicationManagementEntity> wrapper);
	
	List<ApplicationManagementView> selectListView(@Param("ew") Wrapper<ApplicationManagementEntity> wrapper);

	List<ApplicationManagementView> selectListView(Pagination page, @Param("ew") Wrapper<ApplicationManagementEntity> wrapper);
	
	ApplicationManagementView selectView(@Param("ew") Wrapper<ApplicationManagementEntity> wrapper);
	
}
