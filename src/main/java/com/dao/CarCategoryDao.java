package com.dao;

import com.entity.CarCategoryEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.CarCategoryVO;
import com.entity.view.CarCategoryView;


/**
 * 校车类别
 * 
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface CarCategoryDao extends BaseMapper<CarCategoryEntity> {
	
	List<CarCategoryVO> selectListVO(@Param("ew") Wrapper<CarCategoryEntity> wrapper);
	
	CarCategoryVO selectVO(@Param("ew") Wrapper<CarCategoryEntity> wrapper);
	
	List<CarCategoryView> selectListView(@Param("ew") Wrapper<CarCategoryEntity> wrapper);

	List<CarCategoryView> selectListView(Pagination page, @Param("ew") Wrapper<CarCategoryEntity> wrapper);
	
	CarCategoryView selectView(@Param("ew") Wrapper<CarCategoryEntity> wrapper);
	
}
