package com.dao;

import com.entity.CarManagementEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.CarManagementVO;
import com.entity.view.CarManagementView;


/**
 * 校车
 * 
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface CarManagementDao extends BaseMapper<CarManagementEntity> {
	
	List<CarManagementVO> selectListVO(@Param("ew") Wrapper<CarManagementEntity> wrapper);
	
	CarManagementVO selectVO(@Param("ew") Wrapper<CarManagementEntity> wrapper);
	
	List<CarManagementView> selectListView(@Param("ew") Wrapper<CarManagementEntity> wrapper);

	List<CarManagementView> selectListView(Pagination page, @Param("ew") Wrapper<CarManagementEntity> wrapper);
	
	CarManagementView selectView(@Param("ew") Wrapper<CarManagementEntity> wrapper);
	
}
