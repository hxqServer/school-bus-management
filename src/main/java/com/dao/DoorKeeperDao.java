package com.dao;

import com.entity.DoorKeeperEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.DoorKeeperVO;
import com.entity.view.DoorKeeperView;


/**
 * 门卫
 * 
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface DoorKeeperDao extends BaseMapper<DoorKeeperEntity> {
	
	List<DoorKeeperVO> selectListVO(@Param("ew") Wrapper<DoorKeeperEntity> wrapper);
	
	DoorKeeperVO selectVO(@Param("ew") Wrapper<DoorKeeperEntity> wrapper);
	
	List<DoorKeeperView> selectListView(@Param("ew") Wrapper<DoorKeeperEntity> wrapper);

	List<DoorKeeperView> selectListView(Pagination page, @Param("ew") Wrapper<DoorKeeperEntity> wrapper);
	
	DoorKeeperView selectView(@Param("ew") Wrapper<DoorKeeperEntity> wrapper);
	
}
