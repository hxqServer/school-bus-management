package com.dao;

import com.entity.DutyEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.DutyVO;
import com.entity.view.DutyView;


/**
 * 访客管理员
 * 
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface DutyDao extends BaseMapper<DutyEntity> {
	
	List<DutyVO> selectListVO(@Param("ew") Wrapper<DutyEntity> wrapper);
	
	DutyVO selectVO(@Param("ew") Wrapper<DutyEntity> wrapper);
	
	List<DutyView> selectListView(@Param("ew") Wrapper<DutyEntity> wrapper);

	List<DutyView> selectListView(Pagination page, @Param("ew") Wrapper<DutyEntity> wrapper);
	
	DutyView selectView(@Param("ew") Wrapper<DutyEntity> wrapper);
	
}
