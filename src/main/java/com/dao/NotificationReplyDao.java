package com.dao;

import com.entity.NotificationReplyEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.NotificationReplyVO;
import com.entity.view.NotificationReplyView;


/**
 * 通知回复
 * 
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface NotificationReplyDao extends BaseMapper<NotificationReplyEntity> {
	
	List<NotificationReplyVO> selectListVO(@Param("ew") Wrapper<NotificationReplyEntity> wrapper);
	
	NotificationReplyVO selectVO(@Param("ew") Wrapper<NotificationReplyEntity> wrapper);
	
	List<NotificationReplyView> selectListView(@Param("ew") Wrapper<NotificationReplyEntity> wrapper);

	List<NotificationReplyView> selectListView(Pagination page, @Param("ew") Wrapper<NotificationReplyEntity> wrapper);
	
	NotificationReplyView selectView(@Param("ew") Wrapper<NotificationReplyEntity> wrapper);
	
}
