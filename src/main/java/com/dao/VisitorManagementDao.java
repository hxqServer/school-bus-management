package com.dao;

import com.entity.VisitorManagementEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.VisitorManagementVO;
import com.entity.view.VisitorManagementView;


/**
 * 访客
 * 
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface VisitorManagementDao extends BaseMapper<VisitorManagementEntity> {
	
	List<VisitorManagementVO> selectListVO(@Param("ew") Wrapper<VisitorManagementEntity> wrapper);
	
	VisitorManagementVO selectVO(@Param("ew") Wrapper<VisitorManagementEntity> wrapper);
	
	List<VisitorManagementView> selectListView(@Param("ew") Wrapper<VisitorManagementEntity> wrapper);

	List<VisitorManagementView> selectListView(Pagination page, @Param("ew") Wrapper<VisitorManagementEntity> wrapper);
	
	VisitorManagementView selectView(@Param("ew") Wrapper<VisitorManagementEntity> wrapper);
	
}
