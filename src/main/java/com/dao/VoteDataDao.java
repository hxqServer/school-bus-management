package com.dao;

import com.entity.VoteDataEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.VoteDataVO;
import com.entity.view.VoteDataView;


/**
 * 投票信息
 * 
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface VoteDataDao extends BaseMapper<VoteDataEntity> {
	
	List<VoteDataVO> selectListVO(@Param("ew") Wrapper<VoteDataEntity> wrapper);
	
	VoteDataVO selectVO(@Param("ew") Wrapper<VoteDataEntity> wrapper);
	
	List<VoteDataView> selectListView(@Param("ew") Wrapper<VoteDataEntity> wrapper);

	List<VoteDataView> selectListView(Pagination page, @Param("ew") Wrapper<VoteDataEntity> wrapper);
	
	VoteDataView selectView(@Param("ew") Wrapper<VoteDataEntity> wrapper);
	
}
