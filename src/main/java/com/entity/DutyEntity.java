package com.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.beanutils.BeanUtils;


/**
 * 访客管理员
 * 数据库通用操作实体类（普通增删改查）
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@TableName("duty")
public class DutyEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public DutyEntity() {
		
	}
	
	public DutyEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 主键id
	 */
	@TableId
	private Long id;
	/**
	 * 访客账号
	 */
					
	private String originalSchoolAccount;
	
	/**
	 * 密码
	 */
					
	private String password;
	
	/**
	 * 值班日期
	 */
					
	private String responsibleName;
	
	/**
	 * 性别
	 */
					
	private String gender;
	
	/**
	 * 年龄
	 */
					
	private Integer age;
	
	/**
	 * 联系方式
	 */
					
	private String contactInfo;
	
	/**
	 * 备注
	 */
					
	private String remark;
	
	
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat
	private Date addtime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOriginalSchoolAccount() {
		return originalSchoolAccount;
	}

	public void setOriginalSchoolAccount(String originalSchoolAccount) {
		this.originalSchoolAccount = originalSchoolAccount;
	}

	public String getPassword() {
		return password;
	}

	public void setPasswordsword(String password) {
		this.password = password;
	}

	public String getResponsibleName() {
		return responsibleName;
	}

	public void setResponsibleName(String responsibleName) {
		this.responsibleName = responsibleName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getAddtime() {
		return addtime;
	}

	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}
}
