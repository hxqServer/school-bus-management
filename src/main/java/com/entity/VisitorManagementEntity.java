package com.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.beanutils.BeanUtils;


/**
 * 访客
 * 数据库通用操作实体类（普通增删改查）
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@TableName("visitor_management")
public class VisitorManagementEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public VisitorManagementEntity() {
		
	}
	
	public VisitorManagementEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 主键id
	 */
	@TableId
	private Long id;
	/**
	 * 学院名称
	 */
					
	private String originalName;
	
	/**
	 * 资料文件
	 */
					
	private String fileData;
	
	/**
	 * 学院简介
	 */
					
	private String originalDesc;
	
	/**
	 * 职工人数
	 */
					
	private String workerNum;
	
	/**
	 * 访客账号
	 */
					
	private String originalSchoolAccount;
	
	
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat
	private Date addtime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getFileData() {
		return fileData;
	}

	public void setFileData(String fileData) {
		this.fileData = fileData;
	}

	public String getOriginalDesc() {
		return originalDesc;
	}

	public void setOriginalDesc(String originalDesc) {
		this.originalDesc = originalDesc;
	}

	public String getWorkerNum() {
		return workerNum;
	}

	public void setWorkerNum(String workerNum) {
		this.workerNum = workerNum;
	}

	public String getOriginalSchoolAccount() {
		return originalSchoolAccount;
	}

	public void setOriginalSchoolAccount(String originalSchoolAccount) {
		this.originalSchoolAccount = originalSchoolAccount;
	}

	public Date getAddtime() {
		return addtime;
	}

	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}
}
