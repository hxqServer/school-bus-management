package com.entity.view;

import com.entity.ApplicationManagementEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 入校申请
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@TableName("application_management")
public class ApplicationManagementView extends ApplicationManagementEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public ApplicationManagementView(){
	}
 
 	public ApplicationManagementView(ApplicationManagementEntity applicationManagementEntity){
 	try {
			BeanUtils.copyProperties(this, applicationManagementEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
