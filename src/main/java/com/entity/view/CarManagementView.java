package com.entity.view;

import com.entity.CarManagementEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 校车
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@TableName("car_management")
public class CarManagementView extends CarManagementEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public CarManagementView(){
	}
 
 	public CarManagementView(CarManagementEntity carManagementEntity){
 	try {
			BeanUtils.copyProperties(this, carManagementEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
