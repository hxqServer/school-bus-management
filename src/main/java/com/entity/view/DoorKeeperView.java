package com.entity.view;

import com.entity.DoorKeeperEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 门卫
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@TableName("doorkeeper")
public class DoorKeeperView extends DoorKeeperEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public DoorKeeperView(){
	}
 
 	public DoorKeeperView(DoorKeeperEntity doorKeeperEntity){
 	try {
			BeanUtils.copyProperties(this, doorKeeperEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
