package com.entity.view;

import com.entity.NotificationReplyEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 通知回复
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@TableName("notification_reply")
public class NotificationReplyView extends NotificationReplyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public NotificationReplyView(){
	}
 
 	public NotificationReplyView(NotificationReplyEntity notificationReplyEntity){
 	try {
			BeanUtils.copyProperties(this, notificationReplyEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
