package com.entity.view;

import com.entity.VisitorManagementEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 访客
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
@TableName("visitor_management")
public class VisitorManagementView extends VisitorManagementEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public VisitorManagementView(){
	}
 
 	public VisitorManagementView(VisitorManagementEntity visitorManagementEntity){
 	try {
			BeanUtils.copyProperties(this, visitorManagementEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
