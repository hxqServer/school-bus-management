package com.entity.vo;

import java.io.Serializable;
 

/**
 * 门卫
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public class DoorKeeperVO implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 密码
	 */
	
	private String password;
		
	/**
	 * 门卫姓名
	 */
	
	private String userName;
		
	/**
	 * 性别
	 */
	
	private String gender;
		
	/**
	 * 年龄
	 */
	
	private String age;
		
	/**
	 * 个人资料
	 */
	
	private String personalInfo;
		
	/**
	 * 照片
	 */
	
	private String photo;
		
	/**
	 * 联系方式
	 */
	
	private String contactInfo;
		
	/**
	 * 校车类别
	 */
	
	private String unitType;


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getPersonalInfo() {
		return personalInfo;
	}

	public void setPersonalInfo(String personalInfo) {
		this.personalInfo = personalInfo;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
}
