package com.entity.vo;

import java.io.Serializable;
 

/**
 * 访客
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public class VisitorManagementVO implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 资料文件
	 */
	
	private String fileData;
		
	/**
	 * 学院简介
	 */
	
	private String originalDesc;
		
	/**
	 * 职工人数
	 */
	
	private String workerNum;
		
	/**
	 * 访客账号
	 */
	
	private String originalSchoolAccount;


	public String getFileData() {
		return fileData;
	}

	public void setFileData(String fileData) {
		this.fileData = fileData;
	}

	public String getOriginalDesc() {
		return originalDesc;
	}

	public void setOriginalDesc(String originalDesc) {
		this.originalDesc = originalDesc;
	}

	public String getWorkerNum() {
		return workerNum;
	}

	public void setWorkerNum(String workerNum) {
		this.workerNum = workerNum;
	}

	public String getOriginalSchoolAccount() {
		return originalSchoolAccount;
	}

	public void setOriginalSchoolAccount(String originalSchoolAccount) {
		this.originalSchoolAccount = originalSchoolAccount;
	}
}
