package com.entity.vo;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 投票信息
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public class VoteDataVO implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 性别
	 */
	
	private String gender;
		
	/**
	 * 年龄
	 */
	
	private String age;
		
	/**
	 * 个人介绍
	 */
	
	private String personal;
		
	/**
	 * 赞成票
	 */
	
	private Integer approveVote;
		
	/**
	 * 反对票
	 */
	
	private Integer opposeVote;
		
	/**
	 * 更新时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	private Date updateTime;


	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getPersonal() {
		return personal;
	}

	public void setPersonal(String personal) {
		this.personal = personal;
	}

	public Integer getApproveVote() {
		return approveVote;
	}

	public void setApproveVote(Integer approveVote) {
		this.approveVote = approveVote;
	}

	public Integer getOpposeVote() {
		return opposeVote;
	}

	public void setOpposeVote(Integer opposeVote) {
		this.opposeVote = opposeVote;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
