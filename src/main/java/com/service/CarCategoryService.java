package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.CarCategoryEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.CarCategoryVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.CarCategoryView;


/**
 * 校车类别
 *
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface CarCategoryService extends IService<CarCategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<CarCategoryVO> selectListVO(Wrapper<CarCategoryEntity> wrapper);
   	
   	CarCategoryVO selectVO(@Param("ew") Wrapper<CarCategoryEntity> wrapper);
   	
   	List<CarCategoryView> selectListView(Wrapper<CarCategoryEntity> wrapper);
   	
   	CarCategoryView selectView(@Param("ew") Wrapper<CarCategoryEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<CarCategoryEntity> wrapper);
   	
}

