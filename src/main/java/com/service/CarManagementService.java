package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.CarManagementEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.CarManagementVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.CarManagementView;


/**
 * 校车
 *
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface CarManagementService extends IService<CarManagementEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<CarManagementVO> selectListVO(Wrapper<CarManagementEntity> wrapper);
   	
   	CarManagementVO selectVO(@Param("ew") Wrapper<CarManagementEntity> wrapper);
   	
   	List<CarManagementView> selectListView(Wrapper<CarManagementEntity> wrapper);
   	
   	CarManagementView selectView(@Param("ew") Wrapper<CarManagementEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<CarManagementEntity> wrapper);
   	
}

