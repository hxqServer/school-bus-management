package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.DoorKeeperEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.DoorKeeperVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.DoorKeeperView;


/**
 * 门卫
 *
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface DoorKeeperService extends IService<DoorKeeperEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<DoorKeeperVO> selectListVO(Wrapper<DoorKeeperEntity> wrapper);
   	
   	DoorKeeperVO selectVO(@Param("ew") Wrapper<DoorKeeperEntity> wrapper);
   	
   	List<DoorKeeperView> selectListView(Wrapper<DoorKeeperEntity> wrapper);
   	
   	DoorKeeperView selectView(@Param("ew") Wrapper<DoorKeeperEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<DoorKeeperEntity> wrapper);
   	
}

