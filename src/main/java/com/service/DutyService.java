package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.DutyEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.DutyVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.DutyView;


/**
 * 访客管理员
 *
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface DutyService extends IService<DutyEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<DutyVO> selectListVO(Wrapper<DutyEntity> wrapper);
   	
   	DutyVO selectVO(@Param("ew") Wrapper<DutyEntity> wrapper);
   	
   	List<DutyView> selectListView(Wrapper<DutyEntity> wrapper);
   	
   	DutyView selectView(@Param("ew") Wrapper<DutyEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<DutyEntity> wrapper);
   	
}

