package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.NotificationReplyEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.NotificationReplyVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.NotificationReplyView;


/**
 * 通知回复
 *
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface NotificationReplyService extends IService<NotificationReplyEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<NotificationReplyVO> selectListVO(Wrapper<NotificationReplyEntity> wrapper);
   	
   	NotificationReplyVO selectVO(@Param("ew") Wrapper<NotificationReplyEntity> wrapper);
   	
   	List<NotificationReplyView> selectListView(Wrapper<NotificationReplyEntity> wrapper);
   	
   	NotificationReplyView selectView(@Param("ew") Wrapper<NotificationReplyEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<NotificationReplyEntity> wrapper);
   	
}

