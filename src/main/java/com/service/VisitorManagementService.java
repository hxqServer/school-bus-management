package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.VisitorManagementEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.VisitorManagementVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.VisitorManagementView;


/**
 * 访客
 *
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface VisitorManagementService extends IService<VisitorManagementEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<VisitorManagementVO> selectListVO(Wrapper<VisitorManagementEntity> wrapper);
   	
   	VisitorManagementVO selectVO(@Param("ew") Wrapper<VisitorManagementEntity> wrapper);
   	
   	List<VisitorManagementView> selectListView(Wrapper<VisitorManagementEntity> wrapper);
   	
   	VisitorManagementView selectView(@Param("ew") Wrapper<VisitorManagementEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<VisitorManagementEntity> wrapper);
   	
}

