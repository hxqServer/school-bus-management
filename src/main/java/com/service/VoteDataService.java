package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.VoteDataEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.VoteDataVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.VoteDataView;


/**
 * 投票信息
 *
 * @author 
 * @email 
 * @date 2024-03-09 11:06:58
 */
public interface VoteDataService extends IService<VoteDataEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<VoteDataVO> selectListVO(Wrapper<VoteDataEntity> wrapper);
   	
   	VoteDataVO selectVO(@Param("ew") Wrapper<VoteDataEntity> wrapper);
   	
   	List<VoteDataView> selectListView(Wrapper<VoteDataEntity> wrapper);
   	
   	VoteDataView selectView(@Param("ew") Wrapper<VoteDataEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<VoteDataEntity> wrapper);
   	
}

