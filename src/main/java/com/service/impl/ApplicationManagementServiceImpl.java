package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.ApplicationManagementDao;
import com.entity.ApplicationManagementEntity;
import com.service.ApplicationManagementService;
import com.entity.vo.ApplicationManagementVO;
import com.entity.view.ApplicationManagementView;

@Service("notificationSendService")
public class ApplicationManagementServiceImpl extends ServiceImpl<ApplicationManagementDao, ApplicationManagementEntity> implements ApplicationManagementService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ApplicationManagementEntity> page = this.selectPage(
                new Query<ApplicationManagementEntity>(params).getPage(),
                new EntityWrapper<ApplicationManagementEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<ApplicationManagementEntity> wrapper) {
		  Page<ApplicationManagementView> page =new Query<ApplicationManagementView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<ApplicationManagementVO> selectListVO(Wrapper<ApplicationManagementEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public ApplicationManagementVO selectVO(Wrapper<ApplicationManagementEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<ApplicationManagementView> selectListView(Wrapper<ApplicationManagementEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public ApplicationManagementView selectView(Wrapper<ApplicationManagementEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
