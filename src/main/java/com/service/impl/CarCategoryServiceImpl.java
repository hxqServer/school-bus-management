package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.CarCategoryDao;
import com.entity.CarCategoryEntity;
import com.service.CarCategoryService;
import com.entity.vo.CarCategoryVO;
import com.entity.view.CarCategoryView;

@Service("unitTypeService")
public class CarCategoryServiceImpl extends ServiceImpl<CarCategoryDao, CarCategoryEntity> implements CarCategoryService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<CarCategoryEntity> page = this.selectPage(
                new Query<CarCategoryEntity>(params).getPage(),
                new EntityWrapper<CarCategoryEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<CarCategoryEntity> wrapper) {
		  Page<CarCategoryView> page =new Query<CarCategoryView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<CarCategoryVO> selectListVO(Wrapper<CarCategoryEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public CarCategoryVO selectVO(Wrapper<CarCategoryEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<CarCategoryView> selectListView(Wrapper<CarCategoryEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public CarCategoryView selectView(Wrapper<CarCategoryEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
