package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.CarManagementDao;
import com.entity.CarManagementEntity;
import com.service.CarManagementService;
import com.entity.vo.CarManagementVO;
import com.entity.view.CarManagementView;

@Service
public class CarManagementServiceImpl extends ServiceImpl<CarManagementDao, CarManagementEntity> implements CarManagementService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<CarManagementEntity> page = this.selectPage(
                new Query<CarManagementEntity>(params).getPage(),
                new EntityWrapper<CarManagementEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<CarManagementEntity> wrapper) {
		  Page<CarManagementView> page =new Query<CarManagementView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<CarManagementVO> selectListVO(Wrapper<CarManagementEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public CarManagementVO selectVO(Wrapper<CarManagementEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<CarManagementView> selectListView(Wrapper<CarManagementEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public CarManagementView selectView(Wrapper<CarManagementEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
