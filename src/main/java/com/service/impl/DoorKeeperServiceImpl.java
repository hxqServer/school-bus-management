package com.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.dao.DoorKeeperDao;
import com.entity.DoorKeeperEntity;
import com.entity.view.DoorKeeperView;
import com.entity.vo.DoorKeeperVO;
import com.service.DoorKeeperService;
import com.utils.PageUtils;
import com.utils.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class DoorKeeperServiceImpl extends ServiceImpl<DoorKeeperDao, DoorKeeperEntity> implements DoorKeeperService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DoorKeeperEntity> page = this.selectPage(
                new Query<DoorKeeperEntity>(params).getPage(),
                new EntityWrapper<DoorKeeperEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<DoorKeeperEntity> wrapper) {
		  Page<DoorKeeperView> page =new Query<DoorKeeperView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<DoorKeeperVO> selectListVO(Wrapper<DoorKeeperEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public DoorKeeperVO selectVO(Wrapper<DoorKeeperEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<DoorKeeperView> selectListView(Wrapper<DoorKeeperEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public DoorKeeperView selectView(Wrapper<DoorKeeperEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
