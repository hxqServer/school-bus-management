package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.DutyDao;
import com.entity.DutyEntity;
import com.service.DutyService;
import com.entity.vo.DutyVO;
import com.entity.view.DutyView;
@Service
public class DutyServiceImpl extends ServiceImpl<DutyDao, DutyEntity> implements DutyService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DutyEntity> page = this.selectPage(
                new Query<DutyEntity>(params).getPage(),
                new EntityWrapper<DutyEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<DutyEntity> wrapper) {
		  Page<DutyView> page =new Query<DutyView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<DutyVO> selectListVO(Wrapper<DutyEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public DutyVO selectVO(Wrapper<DutyEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<DutyView> selectListView(Wrapper<DutyEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public DutyView selectView(Wrapper<DutyEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
