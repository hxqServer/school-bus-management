package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.NotificationReplyDao;
import com.entity.NotificationReplyEntity;
import com.service.NotificationReplyService;
import com.entity.vo.NotificationReplyVO;
import com.entity.view.NotificationReplyView;

@Service("notificationReplyService")
public class NotificationReplyServiceImpl extends ServiceImpl<NotificationReplyDao, NotificationReplyEntity> implements NotificationReplyService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<NotificationReplyEntity> page = this.selectPage(
                new Query<NotificationReplyEntity>(params).getPage(),
                new EntityWrapper<NotificationReplyEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<NotificationReplyEntity> wrapper) {
		  Page<NotificationReplyView> page =new Query<NotificationReplyView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<NotificationReplyVO> selectListVO(Wrapper<NotificationReplyEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public NotificationReplyVO selectVO(Wrapper<NotificationReplyEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<NotificationReplyView> selectListView(Wrapper<NotificationReplyEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public NotificationReplyView selectView(Wrapper<NotificationReplyEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
