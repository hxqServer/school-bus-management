package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.VisitorManagementDao;
import com.entity.VisitorManagementEntity;
import com.service.VisitorManagementService;
import com.entity.vo.VisitorManagementVO;
import com.entity.view.VisitorManagementView;
@Service
public class VisitorManagementServiceImpl extends ServiceImpl<VisitorManagementDao, VisitorManagementEntity> implements VisitorManagementService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<VisitorManagementEntity> page = this.selectPage(
                new Query<VisitorManagementEntity>(params).getPage(),
                new EntityWrapper<VisitorManagementEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<VisitorManagementEntity> wrapper) {
		  Page<VisitorManagementView> page =new Query<VisitorManagementView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<VisitorManagementVO> selectListVO(Wrapper<VisitorManagementEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public VisitorManagementVO selectVO(Wrapper<VisitorManagementEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<VisitorManagementView> selectListView(Wrapper<VisitorManagementEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public VisitorManagementView selectView(Wrapper<VisitorManagementEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
