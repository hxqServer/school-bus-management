package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.VoteDataDao;
import com.entity.VoteDataEntity;
import com.service.VoteDataService;
import com.entity.vo.VoteDataVO;
import com.entity.view.VoteDataView;

@Service("voteDataService")
public class VoteDataServiceImpl extends ServiceImpl<VoteDataDao, VoteDataEntity> implements VoteDataService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<VoteDataEntity> page = this.selectPage(
                new Query<VoteDataEntity>(params).getPage(),
                new EntityWrapper<VoteDataEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<VoteDataEntity> wrapper) {
		  Page<VoteDataView> page =new Query<VoteDataView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<VoteDataVO> selectListVO(Wrapper<VoteDataEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public VoteDataVO selectVO(Wrapper<VoteDataEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<VoteDataView> selectListView(Wrapper<VoteDataEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public VoteDataView selectView(Wrapper<VoteDataEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
