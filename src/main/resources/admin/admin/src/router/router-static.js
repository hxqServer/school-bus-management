import Vue from 'vue';
//配置路由
import VueRouter from 'vue-router'
Vue.use(VueRouter);
//1.创建组件
import Index from '@/views/index'
import Home from '@/views/home'
import Login from '@/views/login'
import NotFound from '@/views/404'
import UpdatePassword from '@/views/update-password'
import pay from '@/views/pay'
import register from '@/views/register'
import center from '@/views/center'
    import tongzhihuifu from '@/views/modules/tongzhihuifu/list'
    import car_category from '@/views/modules/car_category/list'
    import car_management from '@/views/modules/car_management/list'
    import doorkeeper from '@/views/modules/doorkeeper/list'
    import duty from '@/views/modules/duty/list'
    import toupiaoxinxi from '@/views/modules/toupiaoxinxi/list'
    import visitor_management from '@/views/modules/visitor_management/list'
    import application_management from '@/views/modules/application_management/list'


//2.配置路由   注意：名字
const routes = [{
    path: '/index',
    name: '首页',
    component: Index,
    children: [{
      // 这里不设置值，是把main作为默认页面
      path: '/',
      name: '首页',
      component: Home,
      meta: {icon:'', title:'center'}
    }, {
      path: '/updatePassword',
      name: '修改密码',
      component: UpdatePassword,
      meta: {icon:'', title:'updatePassword'}
    }, {
      path: '/pay',
      name: '支付',
      component: pay,
      meta: {icon:'', title:'pay'}
    }, {
      path: '/center',
      name: '个人信息',
      component: center,
      meta: {icon:'', title:'center'}
    }
          ,{
	path: '/tongzhihuifu',
        name: '通知回复',
        component: tongzhihuifu
      }
          ,{
	path: '/car_category',
        name: '校车类别',
        component: car_category
      }
          ,{
	path: '/car_management',
        name: '校车',
        component: car_management
      }
          ,{
	path: '/doorkeeper',
        name: '门卫',
        component: doorkeeper
      }
          ,{
	path: '/duty',
        name: '门卫值班',
        component: duty
      }
          ,{
	path: '/toupiaoxinxi',
        name: '投票信息',
        component: toupiaoxinxi
      }
          ,{
	path: '/visitor_management',
        name: '访客',
        component: visitor_management
      }
          ,{
	path: '/application_management',
        name: '入校申请',
        component: application_management
      }
        ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {icon:'', title:'login'}
  },
  {
    path: '/register',
    name: 'register',
    component: register,
    meta: {icon:'', title:'register'}
  },
  {
    path: '/',
    name: '首页',
    redirect: '/index'
  }, /*默认跳转路由*/
  {
    path: '*',
    component: NotFound
  }
]
//3.实例化VueRouter  注意：名字
const router = new VueRouter({
  mode: 'hash',
  /*hash模式改为history*/
  routes // （缩写）相当于 routes: routes
})

export default router;
