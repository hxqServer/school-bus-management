const menu = {
    list() {
        return [
            {
                "backMenu": [
                    {
                        "child": [
                            {
                                "buttons": [
                                    "新增",
                                    "查看",
                                    "修改",
                                    "删除"
                                ],
                                "menu": "访客",
                                "menuJump": "列表",
                                "tableName": "visitor_management"
                            }
                        ],
                        "menu": "访客管理"
                    },
                    {
                        "child": [
                            {
                                "buttons": [
                                    "新增",
                                    "查看",
                                    "修改",
                                    "删除"
                                ],
                                "menu": "门卫",
                                "menuJump": "列表",
                                "tableName": "doorkeeper"
                            }
                        ],
                        "menu": "门卫管理"
                    },
                    {
                        "child": [
                            {
                                "buttons": [
                                    "新增",
                                    "查看",
                                    "修改",
                                    "删除"
                                ],
                                "menu": "校车类别",
                                "menuJump": "列表",
                                "tableName": "car_category"
                            }
                        ],
                        "menu": "校车类别管理"
                    },
                    {
                        "child": [
                            {
                                "buttons": [
                                    "新增",
                                    "查看",
                                    "修改",
                                    "删除"
                                ],
                                "menu": "门卫值班",
                                "menuJump": "列表",
                                "tableName": "duty"
                            }
                        ],
                        "menu": "门卫值班管理"
                    },
                    {
                        "child": [
                            {
                                "buttons": [
                                    "新增",
                                    "查看",
                                    "修改",
                                    "删除",
                                    "绑定门卫"
                                ],
                                "menu": "校车",
                                "menuJump": "列表",
                                "tableName": "car_management"
                            }
                        ],
                        "menu": "校车管理"
                    },
                    {
                        "child": [
                            {
                                "buttons": [
                                    "查看",
                                    "修改",
                                    "删除",
                                    "审核"
                                ],
                                "menu": "入校申请",
                                "menuJump": "列表",
                                "tableName": "application_management"
                            }
                        ],
                        "menu": "入校申请管理"
                    }
                ],
                "frontMenu": [],
                "hasBackLogin": "是",
                "hasBackRegister": "否",
                "hasFrontLogin": "否",
                "hasFrontRegister": "否",
                "roleName": "管理员",
                "tableName": "users"
            },
            {
                "backMenu": [
                    {
                        "child": [
                            {
                                "buttons": [
                                    "发送通知",
                                    "查看"
                                ],
                                "menu": "校车",
                                "menuJump": "列表",
                                "tableName": "car_management"
                            }
                        ],
                        "menu": "校车管理"
                    },
                    {
                        "child": [
                            {
                                "buttons": [
                                    "新增",
                                    "查看",
                                    "修改",
                                    "删除"
                                ],
                                "menu": "入校申请",
                                "menuJump": "列表",
                                "tableName": "application_management"
                            }
                        ],
                        "menu": "入校申请管理"
                    },
                    {
                        "child": [
                            {
                                "buttons": [
                                    "查看"
                                ],
                                "menu": "门卫值班",
                                "menuJump": "列表",
                                "tableName": "duty"
                            }
                        ],
                        "menu": "门卫值班列表"
                    }
                ],
                "frontMenu": [],
                "hasBackLogin": "是",
                "hasBackRegister": "否",
                "hasFrontLogin": "是",
                "hasFrontRegister": "是",
                "roleName": "门卫",
                "tableName": "doorkeeper"
            }
        ]
    }
}
export default menu;
